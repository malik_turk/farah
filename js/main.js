$(function(){

    $(".gelleryImage").slice(0, 6).show(); // select the first six
    $("#load").click(function(e){ // click event for load more
        e.preventDefault();
        $(".gelleryImage:hidden").slice(0, 6).show(); // select next 10 hidden divs and show them

    });
});


$(document).ready(function() {
  if ($(".gelleryImage").length < 7) {
    $(".loadmore").hide();
  }
});

function socialMedia(){
  document.write('<footer><div class="socialLight"><div class="text-center ultraLight">Copyright © 2018 Farah Lab. All rights reserved.</div><div id="social"><a href="https://www.facebook.com/Farah-Lab-1117692124963737/"><span class="fa fa-facebook"></span></a><a href="https://www.instagram.com/farahlabs"><span class="fa fa-instagram"></span></a><a href="https://www.linkedin.com/in/farah-lab-915b89170/"><span class="fa fa-linkedin"></span></a></div></div></footer>');

}
